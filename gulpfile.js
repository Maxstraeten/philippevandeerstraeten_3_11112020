const {src, dest, series, parallel, watch} = require('gulp');
const sassGulp = require('gulp-sass');
const fiberGulp = require('fibers');
const purgeCss = require('gulp-purgecss');
const htmlMin = require('gulp-htmlmin');
const surgeGulp = require('gulp-surge');
const imageResize = require('gulp-image-resize');
const imageMin = require('gulp-imagemin');
const imageminMozjpeg = require('imagemin-mozjpeg');
const browserSync = require('browser-sync').create();
const del = require('del');
const reName = require('gulp-rename');
const postCss = require('gulp-postcss');
// POSTCSS plugins
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
const pxtorem = require('postcss-pxtorem');
const mqpacker = require('mqpacker')
// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
sassGulp.compiler = require('sass');


// SASS transform CSS / autoprefixer /purgecss
function sassTask(){
    let plugins = [
        mqpacker(),
        autoprefixer(),
        pxtorem({
            rootValue: 16,
            unitPrecision: 3,
            propList: ['font', 'font-size', 'line-height', 'letter-spacing','padding'],
            selectorBlackList: [],
            replace: true,
            mediaQuery: false,
            minPixelValue: 0,
            exclude: /node_modules/i
        })
    ];
    return src('src/scss/*.scss', {sourcemaps: true})
    .pipe(sassGulp({fiber: fiberGulp}).on('error', sassGulp.logError))
    .pipe(postCss(plugins))
    .pipe(purgeCss({content: ['*.html']}))
    .pipe(dest('src/css', {sourcemaps: "."}));
}

// IMAGE transform crop, size, minify
function imageTask(done){
const sizes = [
    { width: 500, suffix: '-small' },
    { width: 900, suffix: '-medium' },
    { width: 1300, suffix: '-large' },
    { width: 1700, suffix: '-xlarge' },
];
sizes.forEach(function(size) {
    return src('src/img/*.jpg')
    .pipe(imageResize({
        width: size.width,
        height: 0,
        upscale: false
    }))
    .pipe(reName(function(path) {
     path.basename += size.suffix;
    }))
    .pipe(dest('src/img'));
})
done();
}

// FONTS
function fontsTask(){
    return src([
        'node_modules/fontsource-shrikhand/files/shrikhand-latin-400-normal.*',
        'node_modules/fontsource-roboto/files/roboto-latin-400-normal.*',
        'node_modules/fontsource-roboto/files/roboto-latin-700-normal.*',
        'node_modules/@fortawesome/fontawesome-free/webfonts/*'])
        .pipe (dest('src/css/files'));
}

// BROWSER SYNC
function browserSyncServe(cb){
    browserSync.init({
        server:{ baseDir: "."}
        // ou proxy: "yourlocal.dev",
        // port: 3000
    })
    cb();
}

function browserSyncReload(cb){
    browserSync.reload()
    cb();
}

// WATCH css html
 function watchTask(){
    watch("*.html", browserSyncReload);
    watch("src/scss/*.scss", series(
    sassTask,
    browserSyncReload));
}

// PRODUCTION  xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

// DELETE
function cleanProd(){
    return del(['dist/*.html', 'dist/src/css/*.css', 'dist/src/img/*']);
}

// HTML production minify
function htmlProd(){
    return src('*.html')
    .pipe(htmlMin({
        collapseWhitespace: true,
        removeComments: true
    }))
    .pipe(dest('dist'));
}

// CSS production minify
function cssProd(){
    let plugins = [
        cssnano()
    ];
    return src('src/css/*.css')
    .pipe(postCss(plugins))
    .pipe(dest('dist/src/css'));
}

// Images  production minify
function imageProd(){
        return src('src/img/*.jpg')
        .pipe(imageMin([
            imageminMozjpeg({quality: 75, progressive: true})
        ],
        {verbose: true}))
        .pipe(dest('dist/src/img'));
    }


// LOGO production
function iconProd(){
    return src('src/img/logo/*')
    .pipe(imageMin([
            imageMin.optipng({optimizationLevel: 5}),
            imageMin.svgo({
                plugins:[
                    {removeViewBox:true},
                    {cleanupIDs: false}
                ]
             }),
    {verbose: true}]))
    .pipe (dest('dist/src/img/logo'));
}

// FONTS production
function fontsProd(){
    return src('src/css/files/*')
    .pipe (dest('dist/src/css/files'));
}

// DEPLOY Surge production
function deployProd(){
    return surgeGulp({
        project: 'dist',
        domain: 'ohmyfood.surge.sh'
    });
}

// ACTION
exports.watch = series(sassTask, parallel(browserSyncServe, watchTask));
exports.create = imageTask;
exports.build = series(cleanProd, imageProd, iconProd, parallel(htmlProd, cssProd, fontsProd), deployProd);
